function onOpen(){
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('▶チャート表示')
      .addItem('レーダーチャート', 'chartman')
      .addToUi();
}

function chartman() {
  var html = HtmlService.createHtmlOutputFromFile('radar')
    .setSandboxMode(HtmlService.SandboxMode.IFRAME)
    .setWidth(750)
    .setHeight(520);
 
 SpreadsheetApp.getUi() 
    .showModalDialog(html, 'レーダーチャートの作成');
}

function datamanrev(){
  var sheet = SpreadsheetApp.getActiveSpreadsheet();
  var ss = sheet.getSheetByName("radar");
  var dataman = ss.getRange("A1:F").getValues();
  Logger.log(dataman);
  return JSON.stringify(dataman);
}
// TODO:セル選択していろいろしてはこっちに書くもの